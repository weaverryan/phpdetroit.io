Curious about what all this Laravel hype is about? Want to see Rapid Application Development first hand? Tired of your old and busted PHP 5.2 framework?

Join us for a half day of Laravel training. We cover the ecosystem to getting local development to major features and even how to deploy to production.

This class covers Laravel 5.6, application architecture, application testing, best practices, real world implementations, and exercises to put what you learn into action.

Please check out the [set up instructions](https://www.joeferguson.me/php-detroit-tutorial-hit-the-ground-running-with-laravel-set-up/) that you should complete **before** the conference. Please feel free to reach out to [Joe](https://twitter.com/joepferguson) if you have any questions or issues.
